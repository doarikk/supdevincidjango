
from django import forms
from .models import Colis, Entrepot
from django.db.models import F

class ColisForm(forms.ModelForm):
    class Meta:
        model = Colis
        fields = ['reference', 'adresse', 'poids', 'destination', 'date', 'entrepot']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['entrepot'].widget = forms.Select(attrs={'class': 'form-control'})
        # self.fields['entrepot'].queryset = Entrepot.objects.all()  # Fetch all Entrepot instances de ma bdd
        # Cela affichera uniquement les entrepôts où contains est inférieur à chargeMax.
        self.fields['entrepot'].queryset = Entrepot.objects.filter(contains__lt=F('chargeMax')).all()

