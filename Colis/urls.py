from django.urls import path
from .views import blog_index, add_colis, track_colis

urlpatterns = [
    path('', blog_index),
    path('addcolis/', add_colis),
    path('trackcolis/', track_colis),
]