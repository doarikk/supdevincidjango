from django.db import models
from django.utils import timezone

# Create your models here.

class Entrepot(models.Model):
    name = models.CharField(max_length=50)
    contains = models.IntegerField(default=0)
    chargeMax = models.IntegerField(default=10)

    def __str__(self):
        return self.name

class Colis(models.Model):
    reference = models.CharField(max_length=10, default="NA")
    adresse = models.TextField(max_length=50)
    poids = models.IntegerField(default=0)
    destination = models.TextField(max_length=50)
    date = models.DateField(default=timezone.now())
    entrepot = models.ForeignKey(Entrepot, on_delete=models.CASCADE, related_name='colis', null=True)

    def __str__(self):
        return str(self.reference)
