from django.shortcuts import redirect, render, get_object_or_404
from .models import Colis, Entrepot
from .form import ColisForm

# Create your views here.
def blog_index(request):
    # on recupere tous les colis de la bdd
    all_colis = Colis.objects.all()
    return render(request, "index.html", context={'mescolis': all_colis})


def add_colis(request):
    if request.method == 'POST':
        form = ColisForm(request.POST)
        if form.is_valid():
            try:
                entrepotContainValue = get_object_or_404(Entrepot, name=form.cleaned_data['entrepot'])
                print(entrepotContainValue.chargeMax)
                print(entrepotContainValue.contains)
                # il faut mettre a jour le nombre de colis dans entrepot aussi
                entrepotContainValue.contains += 1
                entrepotContainValue.save()
                form.save()            
            except:
                return render(request, "colis-not-found.html")
            return redirect('/colis')
    else:
        form = ColisForm()
    return render(request, 'add-form.html', {'form': form})

# jutilise pas ca
def form_colis(request):
    context = {}
    context['form']= ColisForm()
    return render(request, "add-form.html", context)

def track_colis(request):
    if request.method == 'POST':
        id = request.POST.get('idColis')
        try:
            colis_ids = Colis.objects.values_list('id', flat=True)
            print(colis_ids) # va nous donner la list de tous les id des colis
            # test = get_object_or_404(Colis, reference="C453453453")
            # print(test)
            colis = get_object_or_404(Colis, pk=id)
            print(colis)
            return render(request, "colisDetails.html", context={'moncolis': colis})
        except:
            return render(request, "colis-not-found.html")
    else:
        return render(request, 'colisDetails.html')

    