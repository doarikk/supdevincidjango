from django.contrib import admin

from Colis.models import Colis, Entrepot

# Register your models here.
admin.site.register(Colis)
admin.site.register(Entrepot)