# Generated by Django 5.0.4 on 2024-04-05 09:50

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Colis', '0003_alter_colis_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='colis',
            name='date',
            field=models.DateField(default=datetime.datetime(2024, 4, 5, 9, 50, 24, 39176, tzinfo=datetime.timezone.utc)),
        ),
    ]
