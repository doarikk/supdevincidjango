# Generated by Django 5.0.4 on 2024-04-06 21:13

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Colis', '0009_colis_reference_alter_colis_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='colis',
            name='date',
            field=models.DateField(default=datetime.datetime(2024, 4, 6, 21, 13, 50, 102491, tzinfo=datetime.timezone.utc)),
        ),
        migrations.AlterField(
            model_name='colis',
            name='poids',
            field=models.IntegerField(default=5),
        ),
    ]
