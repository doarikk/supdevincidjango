# Notice d'utilisation

## Afficher la liste des colis :
Pour avoir toute la liste des colis il faut aller sur : http://127.0.0.1:8000/colis  
On affiche la référence de tous les colis.

## Ajouter un colis :
Pour ajouter un nouveau colis aller sur http://127.0.0.1:8000/colis/addcolis 

### Renseigner les différents champs demandés :

1. Référence : par exemple "C000000000"
2. Poids : par exemple 4 (l'unité de mesure est le kg)
3. Adresse : par exemple "22 rue du clos du chêne"
4. Destination : par exemple "3 allée Agatha Christie"
5. Entrepôt :
    - La liste des entrepôts est récupéré depuis la base. On peut choisir parmi les entrepôts où la charge maximum (chargeMax : nombre de colis maximum) n'est pas encore atteinte.
    - Ce calcule se fait directement dans le fichier form.py.


### Les différents champs de Entrepot :
1. name : nom de l'entrepôt.
2. chargeMax : nombre de colis maximum que l'entrepôt peut recevoir. Par défaut l'entrepôt peut avoir une charge maximum de 10 colis.
3. contains : nombre de colis que l'entrepôt possède actuellement. Ce nombre doit être inférieur à la valeur de chargeMax. Lorsqu'on ajoute un colis dans un entrepôt sélectionné, la valeur contains s'auto incrémente de 1.


## Tracker un colis par rapport à son id :
Pour afficher les détails d'un colis par rapport à son id il faut aller sur http://127.0.0.1:8000/colis/trackcolis  
Il faut renseigner l'id (par exemple : 26) dans l'input et les champs du bas sont rafraîchit avec les bonnes valeurs. Si l'id n'est pas trouvé on est renvoyé sur la page d'erreur : Id not found.
